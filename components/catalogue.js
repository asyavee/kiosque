import React from 'react';
import Link from 'next/link';
import { Button } from '@material-ui/core';

class Catalogue extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
    this.fetchStuff = this.fetchStuff.bind(this);
    this.syncCat = this.syncCat.bind(this);
  }

fetchStuff() {

  fetch("http://localhost:3000/api/vkapi")
    .then(res => res.json())
    .then(
      (result) => {
        this.setState({
          isLoaded: true,
          items: result
        });
      },
      // Note: it's important to handle errors here
      // instead of a catch() block so that we don't swallow
      // exceptions from actual bugs in components.
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
  }

syncCat (event) {
  fetch("http://localhost:3000/api/functions");
}

  componentDidMount() {
    this.fetchStuff()
    }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Ошибка: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Загрузка...</div>;
    } else {
      return (
        <ul>

          {items.map((item, index) => (
            <h2 key={index}>
              {item.title}
            </h2>
          ))}
          <Button onClick={this.syncCat} variant='contained'>Sync</Button>
        </ul>
      );
    }
  }
}

export default Catalogue
