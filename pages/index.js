import React from 'react';
import Link from 'next/link';
import Nav from '../components/nav'
import Catalogue from '../components/catalogue'
import Head from 'next/head'
import fetch from 'isomorphic-unfetch';
import { Button, Container } from '@material-ui/core';

const Home = props => (

  <>
    <Nav />
    <Head>
      <title>Home</title>
      </Head>
    <Container>
      <h1>Штуки</h1>
      <Catalogue />
    </Container>
  </>
    );

export default Home;
