const easyvk = require('easyvk');
const mongoose = require('mongoose');
var Products = require('./models.js');

mongoose.connect('mongodb://localhost/kiosquedb', {
  useNewUrlParser: true
}).then(() => {
  console.log("Connected to Database");
}).catch((err) => {
  console.log("Not Connected to Database ERROR! ", err);
});;

//Authorize at vk.com
const auth = easyvk({
  password: "ThymeCntkbfy1",
  username: "89031251333",
  session_file: '.vk-session'
});
const items = auth.then(async vk => {
  let {
    vkr
  } = await vk.call('market.get', {
    owner_id: -184548520,
    extended: 1
  });
  return vkr.items
}).catch(console.error);


//Product composer
function productLego(product) {
  Products.create({
    vk_id: product.id,
    title: product.title,
    description: product.description,
    stock: product.availability,
    price: product.price.amount / 100,
    currency: product.price.currency.name,
    image: product.thumb_photo
  }, (err, item) => {
    if (err) console.log(err)
  })
}

//Create product
function createProduct(product) {
  //Check if this product already was imported
  let item = null
  Products.find({
    vk_id: product.id
  }, 'vk_id', function(err, data) {
    if (err) {
      console.log(err)
    } else item = data
    if (item && item.length) {
      //If there are let me know and do nothing
      console.log(product.title + ', vkid: ' + product.id + ' wasn\'t added because it already exists')
    } else {
      //If there aren't compose the product and let me know it was successfuly created
      productLego(product)
      console.log('added ' + product.title + ', vkid: ' + product.id)
    }
  })
}


//Then store recieved data to DB
export default (req, res) => {
  items.then(items => {
    //Create each product
    items.forEach(product => {
      createProduct(product)
    })
  })
  res.status(204).end()
};
