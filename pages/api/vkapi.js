const easyvk = require('easyvk');
const mongoose = require('mongoose');
var Products = require('./models.js');

mongoose.connect('mongodb://localhost/kiosquedb', {
  useNewUrlParser: true
}).then(() => {
  console.log("Connected to Database");
}).catch((err) => {
  console.log("Not Connected to Database ERROR! ", err);
});;

export default (req, res) => {
  Products.find({}, 'title vk_id title description stock price currency image',
    function(err, data) {
      if (err) {
        console.log(err)
      } else {
        res.setHeader('Content-Type', 'application/json');
        res.statusCode = 200;
        res.end(JSON.stringify(data));
      }
    })
};
