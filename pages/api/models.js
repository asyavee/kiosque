var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const productSchema = new mongoose.Schema({
  vk_id: Number,
  title: String,
  description: String,
  stock: Number,
  price: Number,
  currency: String,
  image: String
});

module.exports = mongoose.model('product');
